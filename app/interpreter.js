'use strict';

const CONSTANT = require('./constant.js');

// turns string command to object 
class Interpreter {

  constructor() {
    this.command_regex = /^(MOVE|LEFT|RIGHT|REPORT|EXIT|PLACE \d,\d,(NORTH|EAST|SOUTH|WEST))$/;
  }

  interpret(msg) {
    var cmd = {};

    msg = msg.toUpperCase().trim();
    if (this.command_regex.test(msg)) {

      var params = msg.split(/,| /);
      if (params[0] in CONSTANT.COMMAND) {

        cmd.status = CONSTANT.STATUS.OK;
        cmd.command = CONSTANT.COMMAND[params[0]];

        if (cmd.command === CONSTANT.COMMAND.PLACE) {
          cmd.x = +params[1];
          cmd.y = +params[2];
          cmd.orientation = CONSTANT.ORIENTATION[params[3]];
        }
      }

    } else {
      cmd.status = CONSTANT.STATUS.ERR;
      cmd.msg = CONSTANT.INVALID_INPUT;
    }

    return cmd;
  }

}

module.exports = Interpreter;
