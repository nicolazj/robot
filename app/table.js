'use strict';

//A robot should be placed on a table

class Table {
  constructor(size) {
    if (!size || size <= 0) {
      throw 'table size should > 0';
    }
    this.size = size;
  }

  isInTable(x, y) {
    return x >= 0 && x < this.size && y >= 0 && y < this.size;
  }
}


module.exports = Table;
