'use strict';

const CONSTANT = require('./constant.js');

var Robot = require('./robot.js');
var Table = require('./table.js');
var Interpreter = require('./interpreter.js');

// robot simulator, 
// receive command from stdin, convert to command object, pass to robot, receive result from robot, display it.
class Simulator {

  constructor() {

    this.table = new Table(5);
    this.robot = new Robot(this.table);


    this.interpreter = new Interpreter();
  }

  bootstrap() {

    var stdin = process.openStdin();

    stdin.addListener('data', (function(d) {

      var msg = d.toString().trim();

      if(msg){
        this.digest(msg);
      }

    }).bind(this));
  }

  digest(msg) {

    var cmd = this.interpreter.interpret(msg);

    if (cmd.status !== CONSTANT.STATUS.OK) {
      console.log(cmd.msg);
    } else {

      switch (cmd.command) {

      case CONSTANT.COMMAND.EXIT:
        console.log('bye!');
        process.exit();
        break;
      default:
        var result = this.robot.input(cmd);
        if (result.status !== CONSTANT.STATUS.OK) {
          console.log(result.msg);
        }else if(cmd.command === CONSTANT.COMMAND.REPORT){
          console.log(result.x+','+result.y+','+Object.keys(CONSTANT.ORIENTATION)[result.orientation]);
        }
        break;
      }
    }
  }
}

module.exports = Simulator;
