'use strict';

// globa constant

var CONSTANT = {
   
  STATUS: {
    OK: 0,
    ERR: -1
  },
  ORIENTATION: {
    'EAST': 0,
    'SOUTH': 1,
    'WEST': 2,
    'NORTH': 3
  },


  COMMAND: {
    'PLACE': 0,
    'MOVE': 1,
    'LEFT': 2,
    'RIGHT': 3,
    'REPORT': 4,
    'EXIT': 5
  },
  INVALID_INPUT:'invalid input'
};

CONSTANT.ORIENTATION_NUM = Object.keys(CONSTANT.ORIENTATION).length,


module.exports = CONSTANT;
