'use strict';

const CONSTANT = require('./constant.js');


// A robot is very simple, take in commands, do something, then return the result.

class Robot {

  constructor(table) {

    if (!table) {
      throw 'a robot should be placed on a table';
    }

    this.table = table;

    this.isPlaced = false;

  }



  input(cmd) {

    if (cmd.command === CONSTANT.COMMAND.PLACE) {
      return this.place(cmd);
    } else if (!this.isPlaced) {
      return {
        status: CONSTANT.STATUS.ERR,
        msg: 'use PLACE X,Y,F to place robot first!'
      };
    } else {
      switch (cmd.command) {
      case CONSTANT.COMMAND.LEFT:
        return this.left();
      case CONSTANT.COMMAND.RIGHT:
        return this.right();
      case CONSTANT.COMMAND.MOVE:
        return this.move();
      case CONSTANT.COMMAND.REPORT:
        return this.report();
      default:
        break;

      }
    }

  }

  place(cmd) {

    var x = cmd.x;
    var y = cmd.y;

    if (this.table.isInTable(x, y)) {
      this.x = cmd.x;
      this.y = cmd.y;
      this.orientation = cmd.orientation;
      this.isPlaced = true;
      return {
        status: CONSTANT.STATUS.OK
      };
    } else {
      return {
        status: CONSTANT.STATUS.ERR,
        msg: 'robot should be placed in table'

      };
    }

  }

  move() {

    var x = this.x;
    var y = this.y;

    switch (this.orientation) {
    case CONSTANT.ORIENTATION.EAST:
      x += 1;
      break;
    case CONSTANT.ORIENTATION.SOUTH:
      y -= 1;
      break;
    case CONSTANT.ORIENTATION.WEST:
      x -= 1;
      break;
    case CONSTANT.ORIENTATION.NORTH:
      y += 1;
      break;
    }
    if (this.table.isInTable(x, y)) {
      this.x = x;
      this.y = y;
      return {
        status: CONSTANT.STATUS.OK
      };
    } else {
      return {
        status: CONSTANT.STATUS.ERR,
        msg: 'out of table, discard the command!'
      };
    }
  }

  right() {
    this.orientation = ++this.orientation % CONSTANT.ORIENTATION_NUM;
    return {
      status: CONSTANT.STATUS.OK
    };
  }

  left() {
    this.orientation = (--this.orientation + CONSTANT.ORIENTATION_NUM) % CONSTANT.ORIENTATION_NUM;
    return {
      status: CONSTANT.STATUS.OK
    };
  }

  report() {

    return {
      status: CONSTANT.STATUS.OK,
      x: this.x,
      y: this.y,
      orientation: this.orientation
    };
  }

}

module.exports = Robot;
