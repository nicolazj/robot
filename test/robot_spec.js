var expect = require('chai').expect;

var CONSTANT = require('../app/constant.js');
var Robot = require('../app/robot.js');
var Table = require('../app/table.js');


describe('Robot', function() {

  it('should instantiate successfully', function() {
    var table = new Table(5);
    var robot = new Robot(table);
    expect(robot.isPlaced).to.be.false;

  });

  describe('should', function() {

    var table, robot;

    beforeEach(function() {

      table = new Table(5);
      robot = new Robot(table);

    });

    it('not do anything before be placed', function() {

      var res = robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.MOVE
      });

      expect(res.status).to.equal(CONSTANT.STATUS.ERR);

    });

    it('be placed if input PLACE command ', function() {

      var res = robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 3,
        y: 2,
        orientation: CONSTANT.ORIENTATION.NORTH
      });

      expect(res.status).to.equal(CONSTANT.STATUS.OK);
      expect(robot.x).to.equal(3);


    });

    it('move correctly case 1', function() {


      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 0,
        y: 0,
        orientation: CONSTANT.ORIENTATION.NORTH
      });

      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.MOVE
      });

      expect(robot.x).to.equal(0);
      expect(robot.y).to.equal(1);

    });

    it('move correctly case 2', function() {

      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 0,
        y: 0,
        orientation: CONSTANT.ORIENTATION.NORTH
      });

      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.LEFT
      });

      expect(robot.orientation).to.equal(CONSTANT.ORIENTATION.WEST);


    });

    it('move correctly case 3', function() {

      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 1,
        y: 2,
        orientation: CONSTANT.ORIENTATION.EAST
      });

      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.MOVE
      });
      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.MOVE
      });
      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.LEFT
      });
      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.MOVE
      });

      expect(robot.x).to.equal(3);
      expect(robot.y).to.equal(3);
      expect(robot.orientation).to.equal(CONSTANT.ORIENTATION.NORTH);


    });

    it('not leave the table', function() {

      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 0,
        y: 0,
        orientation: CONSTANT.ORIENTATION.SOUTH
      });

      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.MOVE
      });

      expect(robot.x).to.equal(0);
      expect(robot.y).to.equal(0);

      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 5,
        y: 6,
        orientation: CONSTANT.ORIENTATION.SOUTH
      });


      expect(robot.x).to.equal(0);
      expect(robot.y).to.equal(0);


      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 4,
        y: 4,
        orientation: CONSTANT.ORIENTATION.EAST
      });
      robot.input({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.MOVE
      });

      expect(robot.x).to.equal(4);
      expect(robot.y).to.equal(4);

    });

  });




});
