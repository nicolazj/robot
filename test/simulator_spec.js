var expect = require('chai').expect;

var CONSTANT = require('../app/constant.js');
var Simulator = require('../app/simulator.js');

describe('Simulator',function(){

  var simulator = new Simulator();

  it('should receive the msg and conduct robot',function(){

    simulator.digest('PLACE 0,0,NORTH');
    expect(simulator.robot.x).to.equal(0);
    expect(simulator.robot.y).to.equal(0);

    simulator.digest('MOVE');
    expect(simulator.robot.y).to.equal(1);

    simulator.digest('LEFT');
    expect(simulator.robot.orientation).to.equal(CONSTANT.ORIENTATION.WEST);

    

  });

});