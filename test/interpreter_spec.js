var expect = require('chai').expect;

var CONSTANT = require('../app/constant.js');
var Interpreter = require('../app/interpreter.js');

describe('Interpreter', function() {

  var interpreter = new Interpreter;

  describe('should parse command correctly', function() {

    it('COMMAND:PLACE', function() {
      var msg = 'PLACE 3,2,NORTH';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 3,
        y: 2,
        orientation: CONSTANT.ORIENTATION.NORTH
      });

      msg = 'PLACE 3,2,EAST';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.PLACE,
        x: 3,
        y: 2,
        orientation: CONSTANT.ORIENTATION.EAST
      });

      msg = 'PLACE 3,2,EA';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.ERR,
        msg: CONSTANT.INVALID_INPUT
      });


      msg = 'PLACE 3,2,';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.ERR,
        msg: CONSTANT.INVALID_INPUT
      });

      msg = 'PLACE a,b,NORTH';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.ERR,
        msg: CONSTANT.INVALID_INPUT
      });

      msg = 'PLACE 1,2,e';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.ERR,
        msg: CONSTANT.INVALID_INPUT
      });



    });

   

    it('COMMAND:MOVE', function() {
      var msg = 'MOVE';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.MOVE
      });

    });


    it('COMMAND:LEFT', function() {
      var msg = 'LEFT';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.LEFT
      });

    });
    it('COMMAND:RIGHT', function() {
      var msg = 'RIGHT';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.RIGHT
      });

    });
    it('COMMAND:REPORT', function() {
      var msg = 'REPORT';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.REPORT
      });

    });
    it('COMMAND:EXIT', function() {
      var msg = 'EXIT';

      expect(interpreter.interpret(msg)).to.deep.equal({
        status: CONSTANT.STATUS.OK,
        command: CONSTANT.COMMAND.EXIT
      });

    });


  });

});
