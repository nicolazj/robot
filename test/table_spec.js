var expect = require('chai').expect;

var Table = require('../app/table.js');

describe('Table', function() {

  it('should instantiate successfully', function() {
    var table = new Table(3);
    expect(table.size).to.equal(3);

  });

  describe('isInTable', function() {

    var table = new Table(5);

    it('should return true if x=1,y=2 when table size is 5', function() {
      expect(table.isInTable(1, 2)).to.be.true;
    });
    it('should return fale if x=5,y=2 when table size is 5', function() {
      expect(table.isInTable(5, 2)).to.be.false;
    });
    it('should return fale if x=-5,y=2 when table size is 5', function() {
      expect(table.isInTable(-5, 2)).to.be.false;
    });

  });

});
