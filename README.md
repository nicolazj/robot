# Robot Simulator

The application is a simulation of a toy robot moving on a square tabletop, of dimensions units x 5 units.

##IMPORTANT:
The source code is ES6 based, please make sure your node version support ES6.(for example: iojs)

## Installation

checkout the code

	npm install

## Run
  
    npm start
Input your command from standard input.

## Test
    npm test
    
## Robot command

### PLACE
	PLACE 2,3,NORTH 
The first valid command to the robot is a PLACE command, after that, any sequence commands may be issued, in any order, including another PLACE command.

### MOVE
	MOVE
### LEFT
	LEFT
### RIGHT
	RIGHT
### REPORT
	REPORT
OUTPUT: 2,3,NORTH

###	 EXIT
	EXIT
	
## HAVE FUN!